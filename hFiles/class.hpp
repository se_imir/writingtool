#ifndef class_hpp
#define class_hpp


class writingSurface{
public:
	std::string space;
	writingSurface();
	~writingSurface();
	void displaySurface();
	
};

//0. Abstraction
//1. Polimorphism
//2. Encapsulation

class abstractwriter{
	float refil;
	std::string color;
public:
	virtual void writing()=0;
	virtual void dispPar()=0;
	virtual void ref(float nr)=0;
};

class baseWriter : public abstractwriter{
	std::string color;
	float refil=10;
public:
	baseWriter();
	~baseWriter();
	void writing();
	void writing(std::string word);
	void dispPar();
	void mnipPar(std::string str);
	void ref(float nr);
	virtual void rR()=0;
	void writingOnSurface(writingSurface *);

};

class pen : public baseWriter{

public:
	pen();
	pen(std::string col);
	~pen();
	void rR();
	
};

class pencil : public baseWriter
{
	float refMax=50;
public:
	pencil();
	~pencil();
	void rR();
	void peel(float nr);
	
	
};

#endif