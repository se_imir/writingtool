#include <iostream>
#include <string.h>
#include "../hFiles/class.hpp"

using namespace std;



int main() {
	pencil olowek;
	pen ballPen;
	pen ballPen2("red");
	
	std::cout<<"\t\t Tools assembled!\n\n";

	ballPen.writing();
	ballPen2.writing("some string");
	ballPen2.~pen();
	ballPen.dispPar();

	
	olowek.writing();
	olowek.writing("some string");
	olowek.~pencil();
	olowek.dispPar();


return 0;
}
