#include <iostream>
#include <string.h>
#include "class.hpp"

baseWriter::baseWriter(){

}

baseWriter::~baseWriter(){

}

void baseWriter::dispPar(){
	std::cout<<"Parameters of the writing tool:\n";
	std::cout<<"Color: "<<this->color<<std::endl;
	std::cout<<"Level of writing medium: "<<this->refil<<std::endl;
}

pen::pen(){
	std::cout<<"Pen was assembled\n Default color was set\n";
	mnipPar("black");
	dispPar();
}

pen::pen(std::string col){
	std::cout<<"Pen was assembled\n "<<col<<" color was set\n";
	mnipPar(col);
	dispPar();
}

pen::~pen(){
	std::cout<<"Object destroyed\n";
}

void baseWriter::writing(){
	if (refil<=0) this->rR();
	std::string tmp;
	std::cout<<"Input string to write: ";
	std::cin.clear();
	std::cin.ignore();
	std::getline(std::cin,tmp);
	std::cout<< "["<<this->color<<"]"<<tmp<<std::endl;
	this->refil=this->refil - (0.01 * (tmp.size()));
}

void baseWriter::writing(std::string col){
	if (refil<=0) this->rR();
	std::cout<< "["<<this->color<<"]"<<col<<std::endl;
	this->refil=this->refil - (0.01 * (col.size()));
}

void baseWriter::mnipPar(std::string str){
	this->color=str;

}

void baseWriter::ref(float nr){
	this->refil=nr;
}

void baseWriter::writingOnSurface(writingSurface *surface){
	if (refil<=0) this->rR();
	std::string tmp;
	std::cout<<"Input string to write: ";
	std::cin.clear();
	std::cin.ignore();
	std::getline(std::cin,surface->space);
	surface->space += "\n";
	this->refil=this->refil - (0.01 * (tmp.size()));
}

void pen::rR(){
	ref(10);

}

void pencil::rR(){
	this->peel(10);
	std::cout <<"can't refil a pencil, peeling instead";
}

void pencil::peel(float nr){
	ref(nr);
	refMax-=nr;
	if (refMax <=0){
		std::cout<<"Tool worn out!";
		this->~pencil();
	}
}

pencil::pencil(){
	std::cout<<"Pencil was assembled\n Default color was set\n";
	mnipPar("black");
	dispPar();
}

pencil::~pencil(){
	std::cout<<"Object destroyed\n";
}

writingSurface::writingSurface(){
	this->space="";
}
writingSurface::~writingSurface(){}

void writingSurface::displaySurface(){
	std::cout<<this->space;
}
